-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 22-Set-2014 às 04:01
-- Versão do servidor: 5.6.11-log
-- versão do PHP: 5.4.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `dbrest`
--

--
-- Extraindo dados da tabela `cidades`
--

INSERT INTO `cidades` (`id`, `name`, `latitude`, `longitude`) VALUES
(3, 'New York', 40.7143528, -74.0059731),
(4, 'Jersey', 40.72788332, -74.07463765),
(5, 'São Paulo', -22.45164882, -49.07592773),
(6, 'Bauru', -22.2992615, -49.03198242),
(7, 'Jaú', -22.26876404, -48.54858398),
(8, 'Peruibe', -24.30705328, -47.0022583),
(9, 'Sorocaba', -23.49851438, -47.44720459),
(10, 'Itu', -23.25648744, -47.28790283),
(11, 'Buri', -23.8004237, -48.59527588),
(12, 'Itapeva', -23.97119535, -48.88092041);

--
-- Extraindo dados da tabela `sequence`
--

INSERT INTO `sequence` (`SEQ_NAME`, `SEQ_COUNT`) VALUES
('SEQ_GEN', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
