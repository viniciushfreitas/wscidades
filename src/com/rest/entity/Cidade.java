package com.rest.entity;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the cidades database table.
 * 
 */

@Table(name="cidades")
@NamedQuery(name="Cidade.findAll", query="SELECT c FROM Cidade c")
@NamedNativeQueries({
	@NamedNativeQuery(
		    name="Cidade.distance",
		    query="SELECT ((((acos(sin((t1.latitude*pi()/180)) * sin((t2.latitude*pi()/180))+cos((t1.latitude*pi()/180)) * cos((t2.latitude*pi()/180)) * cos(((t1.longitude- t2.longitude)* pi()/180))))*180/pi())*60*1.1515) * 1.609344) as distancia "
												+ " FROM (SELECT name, latitude, longitude "
												+ "		  FROM cidades WHERE id = ?) as t1,"
												+ "		 (SELECT name, latitude, longitude "
												+ "		  FROM cidades WHERE id = ?) as t2"
		),
	@NamedNativeQuery(
		    name="Cidade.findCitiesNear",
		    query="SELECT t1.name as nome, t2.name as nomeComparado, t2.id as idComparado, ((((acos(sin((t1.latitude*pi()/180)) * sin((t2.latitude*pi()/180))+cos((t1.latitude*pi()/180)) * cos((t2.latitude*pi()/180)) * cos(((t1.longitude- t2.longitude)* pi()/180))))*180/pi())*60*1.1515) * 1.609344) as distancia"
													+ " FROM (SELECT id, name, latitude, longitude   FROM cidades WHERE id = ?) as t1, cidades as t2 "
													+ " WHERE t1.id <> t2.id"
													+ " ORDER BY distancia"
													+ " LIMIT 0, ?"
		)
})
@Entity
public class Cidade implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Double latitude;
	private Double longitude;
	private String name;
	@Transient
	private String errorMessage;
	
	public Cidade(String errorMessage){
		this.errorMessage = errorMessage;
	}
	public Cidade() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Transient
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}