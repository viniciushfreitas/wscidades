package com.rest.models;

public class Distancia {
	private Double distancia;
	private String errorMessage;
	
	public Distancia(){}
	
	public Distancia(String errorMessage){
		this.errorMessage = errorMessage;
	}
	public Double getDistancia() {
		return distancia;
	}
	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
