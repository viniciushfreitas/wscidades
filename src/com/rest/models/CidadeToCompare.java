package com.rest.models;

public class CidadeToCompare {
	private String nome;
	private String nomeComparado;
	private Integer idComparado;
	private Double distancia;
	private String errorMessage;
	
	public CidadeToCompare(){}
	
	public CidadeToCompare(String errorMessage){
		this.errorMessage = errorMessage;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomeComparado() {
		return nomeComparado;
	}
	public void setNomeComparado(String nomeComparado) {
		this.nomeComparado = nomeComparado;
	}
	public Integer getIdComparado() {
		return idComparado;
	}
	public void setIdComparado(Integer idComparado) {
		this.idComparado = idComparado;
	}
	public Double getDistancia() {
		return distancia;
	}
	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idComparado == null) ? 0 : idComparado.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CidadeToCompare other = (CidadeToCompare) obj;
		if (idComparado == null) {
			if (other.idComparado != null)
				return false;
		} else if (!idComparado.equals(other.idComparado))
			return false;
		return true;
	}
}
