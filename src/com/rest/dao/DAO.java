package com.rest.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.rest.dao.exception.NonexistentEntityException;

public class DAO <T> implements Serializable {
    private EntityManager em = null;
    private Class type = null;
    
    public DAO(Class type, EntityManager emf) {
        this.type = type;
        this.em = emf;
    }

    @Transactional
    public void create(T t) {
        em.persist(t);
    }

    @Transactional
    public void update(T t) {
        t = em.merge(t);
    }

    @Transactional
    public void delete(Long id) throws NonexistentEntityException{
        T t;
        try {
            t = (T) em.getReference(type, id);
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("O objeto com o id: " + id + " n�o existe.", enfe);
        }
        em.remove(t);
    }
    
    @Transactional
    public void delete(T t){
        em.remove(t);
    }
    
    public List<T> findAll() {
        return findAll(true, -1, -1);
    }

    public List<T> findAll(int maxResults, int firstResult) {
        return findAll(false, maxResults, firstResult);
    }

    @Transactional
    private List<T> findAll(boolean all, int maxResults, int firstResult) {
        try{
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(type));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }catch(Exception e) {
            e.printStackTrace();
        }
        
        return null;
    }

    public T find(Long id) {
        return (T) em.find(type, id);
    }

    @Transactional
    public int getCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<T> rt = cq.from(type);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public void close(){
         em.close();
    }
}
