package com.rest.dao;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Query;

import com.rest.entity.Cidade;
import com.rest.models.CidadeToCompare;

public class CidadeDAO extends DAO{
	private static final long serialVersionUID = -869153734174428900L;
	private 
	
	EntityManager em;
    private CidadeDAO(Class type, EntityManager em) {
        super(type, em);
        this.em = em;
    }
    public CidadeDAO(EntityManager em) {
        this(Cidade.class, em);
    }
    
    public List<Cidade> findAll(){
        Query q = em.createNamedQuery("Cidade.findAll");
        
        List<Cidade> list = q.getResultList();
        return list;
    }
    
    public Double distance(long id1, long id2){
    	Query q = em.createNamedQuery("Cidade.distance");
        q.setParameter(1, id1);
        q.setParameter(2, id2);
        
        Double list = (Double) q.getSingleResult();
        
        q.getSingleResult();
        return list;
    }
    
    public List<CidadeToCompare> citiesNear(int n){
    	List<Cidade> cidades =  new ArrayList<Cidade>();
    	List<CidadeToCompare> comparados =  new ArrayList<CidadeToCompare>();
    	int quantidadeLinhas = n;
    	
    	cidades = findAll();
    	if (cidades!=null){
    		List<Cidade> resultados = new ArrayList<Cidade>();
    		for(Cidade cidade : cidades){
    			CidadeToCompare aux = new CidadeToCompare();
    			aux.setIdComparado(cidade.getId());
    			if (!comparados.contains(aux)){
	    			Query query = em.createNamedQuery("Cidade.findCitiesNear");
	    			query.setParameter(1, cidade.getId());
	    			query.setParameter(2, quantidadeLinhas);
	    			
	    			List<Object[]> list = query.getResultList();
	    			for (Object[] obj : list){
	    				CidadeToCompare comparado = new CidadeToCompare();
	    				if (obj[0]!=null){
	    					comparado.setNome((String) obj[0]);
	    				}
	    				if (obj[1]!=null){
	    					comparado.setNomeComparado((String) obj[1]);
	    				}
	    				if (obj[2]!=null){
	    					comparado.setIdComparado((Integer) obj[2]);
	    				}
	    				if (obj[3]!=null){
	    					comparado.setDistancia((Double) obj[3]);
	    				}
	    				comparados.add(comparado);
	    			}
    			}
    		}
    	}
    	CidadeComparator cc = new CidadeComparator();
    	Collections.sort(comparados, cc);
    	return comparados;
    }
    
    private class CidadeComparator implements Comparator<CidadeToCompare> {
        @Override
        public int compare(CidadeToCompare o1, CidadeToCompare o2) {
            if (o1.getDistancia() < o2.getDistancia()){
            	return -1;
            }else if (o1.getDistancia() > o2.getDistancia()){
            	return 1;
            }
        	return 0;
        }
    }
}