package com.rest.resource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;





import com.rest.dao.CidadeDAO;
import com.rest.entity.Cidade;
import com.rest.models.CidadeToCompare;
import com.rest.models.Distancia;

@Path("/cidades")
public class CidadeResource {
    private EntityManager em;
	
	public CidadeResource(){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("WSCidades");
		em =  emf.createEntityManager();
	}
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") 
	public List<Cidade> getCidades() {
		List<Cidade> list;
		try{
			CidadeDAO dao = new CidadeDAO(em);
			return dao.findAll(); 
		}catch(Exception e){
			list = new ArrayList<Cidade>();
			list.add(new Cidade(e.getMessage()));
		}
		return list;
	}
	
	@Path("{id1}/{id2}") 
	@GET 
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") 
	public Distancia getCidade(@PathParam("id1") long id1, @PathParam("id2") long id2) { 
		Distancia distancia;
		try{
			CidadeDAO dao = new CidadeDAO(em);
			distancia = new Distancia();
			distancia.setDistancia(dao.distance(id1, id2));
			return distancia;
		}catch(Exception e){
			distancia = new Distancia(e.getMessage());
		}
		return distancia;
	}
	
	@Path("near")
	@GET 
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") 
	public CidadeToCompare getCitiesNear() {
		CidadeToCompare cidade;
		try{
			CidadeDAO dao = new CidadeDAO(em);
			cidade = dao.citiesNear(1).get(0); 
			return cidade; 
		}catch(Exception e){
			cidade = new CidadeToCompare(e.getMessage());
		}
		return cidade;
	}
}
